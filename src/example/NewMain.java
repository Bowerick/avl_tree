/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import avl_tree.AVLTree;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author Robo
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AVLTree strom = new AVLTree();
        KnihaPodlaNazvu kniha;
        String[] abcd = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                         "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                         "u", "v","w", "x", "y","z"};
        Random gen = new Random();
        
        String nazov = "";;
        for(int i=0;i<100000;i++) {
            nazov = "";
            int length = 5+gen.nextInt(3);
            for(int j=0;j<length;j++)
                nazov += abcd[gen.nextInt(abcd.length)];
            
            kniha = new KnihaPodlaNazvu(new Kniha("Janko", "Hrasko"+i,nazov,i));
            strom.insert(kniha);
        }
        KnihaPodlaNazvu kniha1 = (KnihaPodlaNazvu)strom.search(new KnihaPodlaNazvu(new Kniha(nazov)));
        System.out.println(kniha1.getData());
        System.out.println("_______________________________");
        //7 knih, ktore abecedne nasleduju za 'ro'
        LinkedList<KnihaPodlaNazvu> zoznam = strom.searchInOrder(new KnihaPodlaNazvu(new Kniha("ro")), 7);
        for(KnihaPodlaNazvu k: zoznam)
            System.out.println(k.getData());
    }
    
}
