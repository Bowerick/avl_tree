/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import avl_tree.AVLNode;


/**
 *
 * @author Robo
 */
public class KnihaPodlaNazvu extends AVLNode {
    private Kniha kniha;

    public KnihaPodlaNazvu(Kniha kniha) {
        this.kniha = kniha;
    }
    
    @Override
    public int compare(AVLNode node) {
        if(this.kniha.getNazov().equals( ((KnihaPodlaNazvu) node).getKniha().getNazov() ))
            return 0;
        else {
            if(this.kniha.getNazov().compareTo(((KnihaPodlaNazvu) node).getKniha().getNazov()) < 0)
                return 1;
            else
                return -1;
        }
    }

    @Override
    public String getData() {
        String pom = "";
        pom +=kniha.getMenoAutora()+" "+kniha.getPriezviskoAutora()+", "+kniha.getNazov()+", "+kniha.getId();
        return pom;
    }

    /**
     * @return the kniha
     */
    public Kniha getKniha() {
        return kniha;
    }
}
