/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import avl_tree.AVLNode;


/**
 *
 * @author Robo
 */
public class Kniha extends AVLNode{
    
    private String _menoAutora;
    private String _priezviskoAutora;
    private String _nazov;
    private int _id;
    
    public Kniha(String meno, String priezvisko, String nazov, int id) {
        this._menoAutora = meno;
        this._priezviskoAutora = priezvisko;
        this._nazov = nazov;
        this._id = id;
    }
    
    public Kniha(int id) {
        this._id = id;
    }
    
    public Kniha(String nazovKnihy) {
        this._nazov = nazovKnihy;
    }

    @Override
    public int compare(AVLNode node) {
        if(this.getId() == ((Kniha)node).getId())
            return 0;
        else {
            if(this.getId() < ((Kniha)node).getId())
                return 1;
            else
                return -1;
        }
    }

    @Override
    public String getData() {
        String pom = "";
        pom +=getMenoAutora()+" "+getPriezviskoAutora()+", "+getNazov()+", "+getId();
        return pom;
    }

    /**
     * @return the _id
     */
    public int getId() {
        return _id;
    }

    /**
     * @return the _menoAutora
     */
    public String getMenoAutora() {
        return _menoAutora;
    }

    /**
     * @return the _priezviskoAutora
     */
    public String getPriezviskoAutora() {
        return _priezviskoAutora;
    }

    /**
     * @return the _nazov
     */
    public String getNazov() {
        return _nazov;
    }
    
}
