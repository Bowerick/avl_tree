/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avl_tree;

import java.util.LinkedList;
import java.util.Stack;


/**
 *
 * @author Robo
 */
public class AVLTree {
    private AVLNode root;
    public int pocetPrvkov;
    
    /**
     *
     */
    public AVLTree() {
        root = null;
        pocetPrvkov = 0;
    }
    
    /**
     *
     * @param node
     * @return
     */
    public boolean insert(AVLNode node) {
        if(root == null) {
            root = node;
            pocetPrvkov++;
            return true;
        }
        Stack<AVLNode> stack = new Stack();
        AVLNode temp = root;
        while(temp != null) {
            switch(temp.compare(node)) {
                case 0: 
                    return false;
                case -1: 
                    stack.add(temp);
                    if(temp.getLeft() == null) {
                        temp.setLeft(node);
                        stack.add(node);
                        temp=null;
                        break;//return true;
                    }
                    temp = temp.getLeft();
                    break;
                case 1:
                    stack.add(temp);
                    if(temp.getRight() == null) {
                        temp.setRight(node);
                        stack.add(node);
                        temp=null;
                        break;//return true;
                    }
                    temp = temp.getRight();
                    break;
            }
        }
        pocetPrvkov++;
        //len na inicializaciu, pri prvom prvku nemoze nastat ze treba rotovat
        AVLNode B = stack.peek();
        
        while(!stack.isEmpty()){
            AVLNode N = stack.pop();
            //System.out.println("ooo: "+N);
            
            //N.setFactor();
            N.setHeight();
            /**
            *                             ____
            *                            | 75 |
            *                            |_-1_|
            *                           /      \
            *                          /        \
            *                    ____ /          \ ____ 
            *                   | 50 |            | 90 |
            *                   |_-1_|            |__0_|
            *                  /      
            *                 /        
            *            ____/          \ ____
            *           | 25 |           | 60 |
            *           |__0_|           |____|
            *   pridavam "60"; 
            *   faktor prvku "50" sa zmeni na (-1+1 = 0)
            *   vyska laveho podstromu prvku "75" sa nezmenila 
            *   faktor prvku "75" ostava rovnaky
            */
            if(N.getFactor() == 0 && N.getLeft()!= null && N.getRight()!=null) {
                return true;
            }
            if(Math.abs(N.getFactor()) == 2) {
                AVLNode F = null;
                //rotacia nebola cez root, napoji po rotacii F na B
                if(N.compare(root) != 0) {
                    F = stack.peek();
                }
                
                if( (N.getFactor() == -2)  && (B.getFactor() == -1) ) {
                    RRotation(F,N);
                }
                if( (N.getFactor() == 2)  && (B.getFactor() == 1) ) {
                    LRotation(F,N);
                }
                if( (N.getFactor() == -2)  && (B.getFactor() == 1) ) {
                    LRRotation(F,N);
                }
                if( (N.getFactor() == 2)  && (B.getFactor() == -1) ) {
                    RLRotation(F,N);
                }
                /**
                *                            ____                                 ____
                *                           | F  |                               | F  |
                *                           |____|                               |_  _|
                *                          /      \                             /      \
                *                         /        \                           /        \
                *                    ____/          \                     ____/          \ 
                *                   |  N |                               |  B |   
                *                   |_-2_|                               |__0_|            
                *                  /                                    /      \
                *                 /                                    /        \
                *            ____/                                ____/          \ ____
                *           |  B |                        =>     |    |           |  N |       
                *           |_-1_|                               |____|           |__0_| 
                *         /       \                            /                 /
                *    ____/         \ ____                                   ____/
                *   |    |          |  X |                                 |  X |
                *   |____|          |____|                                 |____|
                * napojit po rotacii na zvysok stromu, ak otec nebol root
                */
                return true;
            }
            B = N;
        }
        return true;
    }
    
    /**
     *
     * @param node
     * @return
     */
    public AVLNode search(AVLNode node) {
        AVLNode temp = getRoot();
        while(temp != null) {
            switch(temp.compare(node)) {
                case 0: 
                    return temp;
                case -1: 
                    temp = temp.getLeft();
                    break;
                case 1: 
                    temp = temp.getRight();
                    break;
            }
        }
        return null;
    }
    
    //k1ed objekt vymazem zo stromu ostanu mu referencie na synov a ked ho znova vlozim,....
    public boolean remove2(AVLNode node) {
        boolean pom = remove(node);
        node.setLeft(null);
        node.setRight(null);
        if(pom) 
            pocetPrvkov--;
        return pom;
    }
    
    /**
     *
     * @param node
     * @return 
     */
    public boolean remove(AVLNode node) {
        if(root == null) {
            return false;
        }
        Stack<AVLNode> stack = new Stack();
        AVLNode temp = root;
        AVLNode temp2 = root;
        while(temp != null) {
            switch(temp.compare(node)) {
                case 0: 
                    stack.add(temp);
                    temp2 = temp;
                    temp = null;
                    break;
                case -1: 
                    stack.add(temp);
                    temp2 = temp;
                    temp = temp.getLeft();
                    if(temp==null)
                        return false;
                    break;
                case 1:
                    stack.add(temp);
                    temp2 = temp;
                    temp = temp.getRight();
                    if(temp==null)
                        return false;
                    break;
            }
        }
        AVLNode N = temp2;
        AVLNode B = stack.peek();
        AVLNode FB = stack.peek();
        AVLNode FN = stack.peek();
        int index = -1;
        //neodstranujem koren - nastav otca
        if(N.compare(root) != 0) {
            index = stack.indexOf(N);
            FN = stack.get(index-1);
        }else {
            //mazem root
            index = 0;
        }
        //odstran mazaneho zo stacku
        stack.pop();
        //odstranovany nema potomkov alebo ma 1 potomka
        if(N.getLeft() == null || N.getRight() == null) {
            //odstranovany nema potomkov
            if(N.getLeft() == null && N.getRight() == null) {
                //odstran - nastav otcovi null
                if(N.compare(root) != 0) {
                    //ak odstranovany nebol root
                    if(FN.getLeft() != null)
                        if(FN.getLeft().compare(N) == 0)
                            FN.setLeft(null);
                    if(FN.getRight() != null)
                        if(FN.getRight().compare(N) == 0)
                            FN.setRight(null);
                } else {
                    //ak bol odstranovany root
                    root = null;
                    return true;
                }
                //return true;
            } 
            //odstranovany ma 1 potomka
            else {
                if(N.getLeft() != null) 
                    B = N.getLeft();
                else
                    B = N.getRight();
                //odstran - nastav otcovi noveho syna
                if(N.compare(root) != 0) {
                    //ak odstranovany nebol root
                    if(FN.getLeft() != null)
                        if(FN.getLeft().compare(N) == 0)
                            FN.setLeft(B);
                    if(FN.getRight() != null)
                        if(FN.getRight().compare(N) == 0)
                            FN.setRight(B);
                }
                else {
                    //ak bol odstranovany root
                    root = B;
                }
            }
        }
        //odstranovany ma 2 potomkov
        if(N.getLeft() != null && N.getRight() != null) {
            //najdem nasledovnika(B) = najlavejsi v pravom podstrome
            FB = N;
            B = N.getRight();
            //nasledovnik nema laveho syna, ale moze mat praveho:
            //napoj nasledovnikovmu otcovi laveho syna nasledovnikovym pravym
            temp = B;
            stack.add(temp);//
            while(temp.getLeft() != null) {
                FB = B;//otec nasledovnika
                B = temp.getLeft();
                temp = temp.getLeft();
                stack.add(temp);//posledny vrchol nepridavam, lebo ten nahradi mazany a uz nebude na povodnom mieste
            }
            //vloz nasledovnika do stacku na miesto mazaneho
            if(index > stack.size()  || index < 0 )
                System.out.println(index+"   "+stack.size());
            stack.add(index, B);
            // ak mal nasledovnik syna uloz ho a neskor ho napoj do stromu : FB.setLeft(temp);
            // teraz nemozem napojit lebo ak nasledovnij je pravy syn mazaneho tak FB je mazany a tomu by som nastavil prave syna nasledovnika
            temp = B.getRight();           
            //napoj naslednika na nove miesto, kde bol mazany
            // nastav nasledovnikovy synov mazaneho
            //ak nassledovnik je pravy syn mazaneho nenastavuj, lebo uz ma
            if(N.getRight().getLeft() != null) {
                FB.setLeft(temp);//!!!!!!!!!
                B.setRight(N.getRight());
            }
            B.setLeft(N.getLeft());
            //ODSTRAN NASLEDOVNIKA
            stack.pop();
            if(N.compare(root) != 0) {
                if(FN.getLeft() != null)
                        if(FN.getLeft().compare(N) == 0)
                            FN.setLeft(B);
                if(FN.getRight() != null)
                    if(FN.getRight().compare(N) == 0)
                        FN.setRight(B);
            } 
            else {
                root = B;
            }
        }
        
        
        
        while(!stack.isEmpty()) {
            N = stack.pop();
            if(N.compare(root) != 0)
                FN = stack.peek();
            if(N.getFactor() == 0) {
                N.setHeight();
                return true;
            }
            N.setHeight();           
            if(Math.abs(N.getFactor()) > 1) {
                if( (N.getFactor() > 1)  ) {
                    //najdi syna N s vacsou height
                    // lavy nemoze byt nikdy null(nahradzam ), ale ak pravy syn mazaneho nemal synov, tak po vymazani bude mat praveho null
                    if(N.getRight() == null)
                        B = N.getLeft();
                    else {
                        if(N.getLeft() == null)
                            B = N.getRight();
                        else {
                            //vyber syna s vacsou height
                            if(N.getLeft().getHeight() > N.getRight().getHeight()) {
                                B = N.getLeft();
                            } else {
                                B = N.getRight();
                            }
                        }
                    }
                    if(B.getFactor() < 0) {
                        RLRotation(FN,N);
                    }
                    else {
                        LRotation(FN,N);
                    }
                } else {
                    if( (N.getFactor() < -1)  ) {
                        if(N.getLeft() == null)
                            B = N.getRight();
                        else {
                            if(N.getRight() == null) {
                                B = N.getLeft();
                            } else {
                                if(N.getLeft().getHeight() > N.getRight().getHeight()) {
                                    B = N.getLeft();
                                }
                                else {
                                    B = N.getRight();
                                }
                            }
                        }
                        if(B.getFactor() > 0) {
                            LRRotation(FN,N);
                        }
                        else
                            RRotation(FN,N);
                    }
                }
            }
            
            B = N;
        }
        return true;
    }
    
    //jednoducha lava
    private void LRotation(AVLNode F, AVLNode N) {
        AVLNode B =N.getRight();
        N.setRight(B.getLeft());
        B.setLeft(N);
        
        N.setHeight();
        B.setHeight();
        
        //rotacia cez root
        if(N.compare(root) == 0) {
            root = B;
        } else { 
//            F.setHeight();
//            F.setFactor();
            if(F.getLeft() != null && F.getLeft().compare(N) == 0)
                F.setLeft(B);
            else 
                F.setRight(B);
        }
    }
    
    //jednoducha prava
    private void RRotation(AVLNode F, AVLNode N) {
        AVLNode B =N.getLeft();
        N.setLeft(N.getLeft().getRight());
        B.setRight(N);
        
        N.setHeight();
        B.setHeight(); 
        
        //rotacia cez root
        if(N.compare(root) == 0) {
            root = B;
        } else {
//            F.setHeight();
//            F.setFactor();
            if(F.getLeft() != null && F.getLeft().compare(N) == 0)
                F.setLeft(B);
            else
                F.setRight(B);
        }
    }
    
    private void LRRotation(AVLNode F, AVLNode N) {
        AVLNode B = N.getLeft();
        LRotation(N, B);
        RRotation(F,N);
    }
    private void RLRotation(AVLNode F, AVLNode N) {
        AVLNode B = N.getRight();
        RRotation(N,B);
        LRotation(F,N);
        
    }

    /**
     * @return the root
     */
    public AVLNode getRoot() {
        return root;
    }
    
    public LinkedList inOrder(AVLNode node) {
        LinkedList list = new LinkedList();
        Stack<AVLNode> stack = new Stack();
        while(!stack.isEmpty() || node != null) {
            if(node != null) {
                stack.push(node);
                node = node.getLeft();
            } else {
                node = stack.pop();
                list.add(node);
                node = node.getRight();
            }
        }
        return list;
    }
    
    public LinkedList levelOrder() {
        LinkedList<AVLNode> queue = new LinkedList();
        LinkedList<AVLNode> list = new LinkedList();
        if(root == null)
            return list;
        queue.add(root);
        AVLNode node;
        while(!queue.isEmpty()) {
            node = queue.pollFirst();
            list.add(node);
            if(node.getLeft() != null)
                queue.add(node.getLeft());
            if(node.getRight() != null)
                queue.add(node.getRight());
        }
        return list;
    }

    //metoda na vypis nasledujucich knih
    public LinkedList searchInOrder(AVLNode node, int pocetK) {
        LinkedList list = new LinkedList();
        if(pocetK ==0)
            return list;
        int pocetNajdenych =0;
        if(root == null) {
            return null;
        }
        Stack<AVLNode> stack = new Stack();
        AVLNode temp = root;
        //ulozim si cestu k hladanej knihe
        while(temp != null) {
            switch(temp.compare(node)) {
                case 0: 
                    return null;
                case -1: 
                    stack.add(temp);
                    if(temp.getLeft() == null) {
                        temp=null;
                        break;
                    }
                    temp = temp.getLeft();
                    break;
                case 1:
                    stack.add(temp);
                    if(temp.getRight() == null) {
                        temp=null;
                        break;//return true;
                    }
                    temp = temp.getRight();
                    break;
            }
        }
        AVLNode F;
        AVLNode S = node;//syn F
        LinkedList<AVLNode> l = new LinkedList();
        while(!stack.isEmpty()) {
            F =stack.pop();
            //ak je lavy syn, vloz ho do najdenych aj s jeho pripadnym pravym synom-inOrder
            if(F.compare(S)==-1) {
                list.add(F);
                pocetNajdenych++;
                if(pocetNajdenych == pocetK)
                    return list;
                //ak ma otec praveho syna, sprav nad nim inOrder a vloz vsetky prvky do najdenych
                if(F.getRight() != null) {
                    l = inOrder(F.getRight());
                    for(AVLNode n: l) {
                        list.add(n);
                        pocetNajdenych++;
                        if(pocetNajdenych == pocetK)
                            return list;
                    }
                }
            } else {
                //ked je pravy, nevkladaj nic prejdi na dalsieho potomka
            }
            S = F;   
        }
        return list;
    }
}
