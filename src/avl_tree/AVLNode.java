/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avl_tree;

/**
 *
 * @author Robo
 */
public abstract class AVLNode {
    private AVLNode left;
    private AVLNode right;
    private int height;
    
    public AVLNode() {
        
    }
    
    public abstract int compare(AVLNode node);
    public abstract String getData();

    /**
     * @return the left
     */
    public AVLNode getLeft() {
        return left;
    }

    /**
     * @return the right
     */
    public AVLNode getRight() {
        return right;
    }

    /**
     * @param left the left to set
     */
    public void setLeft(AVLNode left) {
        this.left = left;
    }

    /**
     * @param right the right to set
     */
    public void setRight(AVLNode right) {
        this.right = right;
    }

    /**
     * @return the factor
     */
    public int getFactor() {
        int l,r;
        if(this.getLeft() == null)
            l = 0;
        else
            l = getLeft().getHeight()+1;
        if(this.getRight() == null)
            r = 0;
        else
            r = getRight().getHeight()+1;
        return - l + r;
    }
    
    public void setHeight() {
        int l,r;
        if(this.getLeft() == null)
            l = -1;
        else
            l = getLeft().getHeight();
        if(this.getRight() == null)
            r = -1;
        else
            r = getRight().getHeight();
        this.height = Math.max(l+1,r+1);
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

}
